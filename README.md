# Description

Source: https://gitlab.com/thelabnyc/wagtail_blog/-/tree/master/blog

## Environment variable

- DJANGO_DATABASE_URL
- DJANGO_LANGUAGE_CODE
- DJANGO_TIME_ZONE
- DJANGO_POST_PAGINATION_PER_PAGE

## Paths

- /app/media - this stores files uploaded from outside (own is 1000:1000)

# Init

```console
python3 -m venv .venv
```
